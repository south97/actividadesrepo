package com.example.demoCrud.repository;

import com.example.demoCrud.modelo.actividades;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface actividadesRepository extends CrudRepository<actividades,Long> {

    actividades findByNombreactividad(String nombreactividad);
    actividades findByProfesorAndAsignatura(String profesor, String asignatura);

}

