package com.example.demoCrud.controller;

import com.example.demoCrud.dto.genericDto;
import com.example.demoCrud.modelo.actividades;
import com.example.demoCrud.services.actividadesServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/actividades")

public class actividadesController {
    @Autowired
    private actividadesServices ActividadesServices;


    @GetMapping(path = "/obtenerActividades")
    public ResponseEntity<genericDto> obtenerActividades() {
        return ResponseEntity.ok().body(genericDto.sucess(this.ActividadesServices.obtenerActividades()));
    }

    @PostMapping(path = "/guardarActividad")
    public ResponseEntity<genericDto> guardarActividad(@RequestBody actividades actividad) {
        return ResponseEntity.ok().body(genericDto.sucess(this.ActividadesServices.guardarActividad(actividad)));
    }

    @PostMapping(path = "/eliminarActividad")
    public ResponseEntity<genericDto> eliminarActividad(@RequestBody actividades actividad) {
        return ResponseEntity.ok().body(genericDto.sucess(this.ActividadesServices.eliminarActividad(actividad)));
    }

    @GetMapping(path = "/obtenerActividad")
    public ResponseEntity<genericDto> obtenerActividad(@RequestParam("nombre") String nombre) {
        return ResponseEntity.ok().body(genericDto.sucess(this.ActividadesServices.nombreActividad(nombre)));
    }

    @GetMapping(path = "/obtenerActividadByProfesor")
    public ResponseEntity<genericDto> obtenerActividadByProfesor(@RequestParam("profesor") String profesor,
                                                                 @RequestParam("asignatura") String asignatura) {
        return ResponseEntity.ok().body(genericDto.sucess(this.ActividadesServices.proAs(profesor,asignatura)));
    }
}
