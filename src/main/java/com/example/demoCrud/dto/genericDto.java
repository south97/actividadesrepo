package com.example.demoCrud.dto;

import lombok.Data;
import org.springframework.http.HttpStatus;

@Data
public class genericDto {

    public Integer status;
    public Object payload;

    public static genericDto sucess(Object data) {

        genericDto GenericDto = new genericDto();
        GenericDto.status=(HttpStatus.OK.value());
        GenericDto.payload=(data);

        return GenericDto;
    }


    public static genericDto failed(Object data) {
        genericDto GenericDto = new genericDto();
        GenericDto.status=(HttpStatus.INTERNAL_SERVER_ERROR.value());
        GenericDto.payload=(data);

        return GenericDto;
    }

}
