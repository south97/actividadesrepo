package com.example.demoCrud.services;

import com.example.demoCrud.modelo.actividades;

import java.util.List;

public interface actividadesServices {
    List<actividades> obtenerActividades();

    String guardarActividad(actividades actividad);
    String eliminarActividad(actividades actividad);

    actividades nombreActividad(String nombre);
    actividades proAs(String profesor, String asignatura);
}


