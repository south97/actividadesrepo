package com.example.demoCrud.services.impl;

import com.example.demoCrud.modelo.actividades;
import com.example.demoCrud.repository.actividadesRepository;
import com.example.demoCrud.services.actividadesServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class servicesImpl implements actividadesServices {
    @Autowired
    actividadesRepository _actividadesRepository;
    @Override
    public List<actividades> obtenerActividades() {

        List<actividades> todos = (List<actividades>) _actividadesRepository.findAll();
        return todos;
    }

    @Override
    public String guardarActividad(actividades actividad) {
    _actividadesRepository.save(actividad);
        return "guardado";
    }

    @Override
    public String eliminarActividad(actividades actividad) {
        _actividadesRepository.delete(actividad);
        return "eliminado";
    }

    @Override
    public actividades nombreActividad(String nombre) {
        return _actividadesRepository.findByNombreactividad(nombre);
    }

    @Override
    public actividades proAs(String profesor, String asignatura) {
        return _actividadesRepository.findByProfesorAndAsignatura(profesor,asignatura);
    }
}
