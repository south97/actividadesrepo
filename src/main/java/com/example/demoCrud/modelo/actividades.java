package com.example.demoCrud.modelo;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@Entity
@Table(name = "Actividades")

public class actividades {
    @Id
    String nombreactividad;
    String descripcion;
    String asignatura;
    String profesor;
}
